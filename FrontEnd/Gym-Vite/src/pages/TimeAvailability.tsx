// TimeAvailability.tsx

import * as React from 'react';
import { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import BottomNavBar from "../components/BottomNavBar.tsx";
import {Modal, Spinner} from 'react-bootstrap';

interface Timeslot {
  date: string;
  startTime: string;
  endTime: string;
}


const TimeAvailability: React.FC = () => {
  const [selectedDay, setSelectedDay] = useState<number | null>(null);
  const [timeslots, setTimeslots] = useState<Timeslot[]>([]);
  // const [currentDate, setCurrentDate] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false); // New state for loading

  useEffect(() => {
    // Set the current date and initialize the timeslots state
    // const today = new Date();
    // setCurrentDate(today.toISOString().split('T')[0]);

    // const initialTimeslots: Timeslot[] = Array.from({ length: 7 }, (_, index) => ({
    //   userId: 5, // replace with the actual user ID
    //   date: today.toISOString().split('T')[0],
    //   startTime: '00:00:00',
    //   endTime: '00:00:00',
    // }));

    // setTimeslots(initialTimeslots);
  }, []);


  const handleDayClick = (dayIndex: number) => {
    console.log('Selected Day:', dayIndex);
    setSelectedDay(dayIndex);
  };

  const handleTimeChange = (field: 'startTime' | 'endTime', value: string) => {
    if (selectedDay !== null) {
      setTimeslots((prevTimeslots) => {
        const updatedTimeslots = [...prevTimeslots];
        const existingSlot = updatedTimeslots[selectedDay];

        if (existingSlot) {
          updatedTimeslots[selectedDay] = {
            ...existingSlot,
            [field]: value || '00:00:00',
          };
        } else {
          // If the slot doesn't exist, create a new one with the date corresponding to the selected day
          const day = new Date();
          day.setDate(day.getDate() + selectedDay);
          const selectedDate = day.toISOString().split('T')[0];

          updatedTimeslots[selectedDay] = {
            date: selectedDate,
            startTime: field === 'startTime' ? value || '00:00:00' : '',
            endTime: field === 'endTime' ? value || '00:00:00' : '',
          };
        }

        return updatedTimeslots;
      });
    }
  };



  const handleSubmit = async () => {
    const token = localStorage.getItem('token');

    // Filter out undefined elements and timeslots without startTime and endTime
    const validTimeslots = timeslots.filter(slot => slot && slot.startTime && slot.endTime);
    console.log(JSON.stringify({ timeslots: validTimeslots }));

    if (validTimeslots.length > 0) {
      setLoading(true); // Set loading to true before making the request

      try {
        const response = await fetch('http://gymmate.pythonanywhere.com/backgpt/available', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`,
          },
          body: JSON.stringify({ timeslots: validTimeslots }),
        });

        if (response.ok) {
          console.log('Availability submitted successfully!');

          // If the availability is submitted successfully, send a GET request to the /plan API
          try {
            const planResponse = await fetch('http://gymmate.pythonanywhere.com/backgpt/plan', {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Token ${token}`,
              },
            });

            if (planResponse.ok) {
              const planData = await planResponse.json();
              console.log('Plan data:', planData);
              // Process the plan data as needed
              window.location.href = '/schedule';
            } else {
              console.error('Error fetching plan data.');
            }
          } catch (error) {
            console.error('Error fetching plan data:', error);
          }
        } else {
          console.error('Error submitting availability.');
        }
      } catch (error) {
        console.error('Error submitting availability:', error);
      } finally {
        setLoading(false); // Set loading to false after the request is complete
      }
    } else {
      console.error('No valid availability to submit.');
    }
  };



  const shouldShowNextWeekAvailability = () => {
    const today = new Date();
    return today.getDay() === 0 && today.getHours() >= 20;
  };

  const remainingDays = Array.from({ length: 7 }, (_, index) => {
    const day = new Date();
    day.setDate(day.getDate() + index + (1 - day.getDay()) % 7); // Set the initial day to Monday
    return day;
  }).filter(day => day >= new Date());

  return (
    <div className="container mt-4">
      <h3>Select a day to input availability until Sunday:</h3>
      <div className="list-group mb-4">
        {remainingDays.map((day, index) => {
          const formattedDate = day.toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric' });

          return (
            <button
              key={index}
              type="button"
              className={`list-group-item list-group-item-action ${selectedDay === index ? 'active' : ''}`}
              onClick={() => handleDayClick(index)}
            >
              {formattedDate}
            </button>
          );
        })}
      </div>

      {shouldShowNextWeekAvailability() ? (
        <div className="mb-4">
          <h4>Input availability for the next week:</h4>
          {/* Add your UI elements for inputting availability for the next week here */}
        </div>
      ) : (
        <>
          {selectedDay !== null && (
            <div className="mb-3">
              <h5>{`Availability for ${timeslots[selectedDay]?.date}`}</h5>
              <div className="mb-3">
                <label htmlFor="startTime" className="form-label">Start Time</label>
                <input
                  type="time"
                  className="form-control"
                  id="startTime"
                  value={timeslots[selectedDay]?.startTime || ''}
                  onChange={(e) => handleTimeChange('startTime', e.target.value)}
                  required
                />
              </div>
              <div className="mb-3">
                <label htmlFor="endTime" className="form-label">End Time</label>
                <input
                  type="time"
                  className="form-control"
                  id="endTime"
                  value={timeslots[selectedDay]?.endTime || ''}
                  onChange={(e) => handleTimeChange('endTime', e.target.value)}
                  required
                />
              </div>
            </div>
          )}

          <button
            type="button"
            className="btn btn-primary"
            onClick={handleSubmit}
          >
            Submit for the week
          </button>
          {/* Loading Popup */}
          <Modal show={loading} backdrop="static" keyboard={false}>
            <Modal.Body className="text-center">
              <Spinner animation="border" variant="primary" />
              <p>Loading...</p>
            </Modal.Body>
          </Modal>
        </>
      )}
        <BottomNavBar/>
    </div>

  );
}

export default TimeAvailability;